﻿using BusinessObject.Models;
using DAL;

namespace BAL
{
    public class Bal
    {
        Dal dal = new Dal();
        public int AddProduct(Product product)
        {
            dal.AddProduct(product);
            return 1;
        }

        public int DeleteProduct(int id) {
        dal.DeleteProduct(id);
            return 1;
        }
        //public Role GetUserRole(string roleName)
        //{
        //    return dal.GetUserRole(roleName);
        //}

        public int UpdateProduct(string name, Product product)
        {
            dal.UpdateProduct(name, product);


            return 1;

        }

        public int AddUser(User user)
        {
            dal.AddUser(user);
            return 0;
        }

        public int DeleteUser(int id)
        {
            dal.DeleteUser(id);
            return 1;
        }

        public int AddOrder(Order order)
        {
            dal.AddOrder(order);
            return 0;
        }


        //public int DisplayProduct(Product product)
        //{
        //    dal.DisplayProduct();
        //    return 0;
        //}

        public List<Product> GetProducts()
        {
            return dal.GetProducts();
        }



        public int PlaceOrder(Order order)
        {
            return dal.PlaceOrder(order);
        }



        public List<User> GetUsers()
        {
            return dal.GetUsers();
        }

    }
}