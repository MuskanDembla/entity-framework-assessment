﻿using BusinessObject.Models;
using DAL.Context;
using System.Runtime.InteropServices;

namespace DAL
{
    public class Dal
    {
        ShoppingDbContext db = new ShoppingDbContext();
        public int AddProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
            return 1;
        }
        //public Role GetUserRole(int id)
        //{
        //    return db.Roles.FirstOrDefault(x => x.RoleName == roleName);
        //}

        public int DeleteProduct(int id)
        {
            Product obj = db.Products.Where(x => x.ProductId == id).FirstOrDefault();
            if (obj != null)
            {
                db.Products.Remove(obj);
                db.SaveChanges();
                Console.WriteLine("Product has been deleted");
                return 0;
            }
            else
            {
                Console.WriteLine("No such product exists");
                return 1;
            }
        }

        public int EditProduct(int id, Product product)
        {


            return 1;

        }


        public int AddUser(User user)
        {
            List<Role> Roles = db.Roles.ToList();
            Console.WriteLine(Roles);
            Console.WriteLine("Choose role id:");
            int RoleId = Convert.ToInt32(Console.ReadLine());



            Role obj = db.Roles.Find(RoleId);
            user.Role = obj;
            user.Role.RoleId = RoleId;
            db.Users.Add(user);
            db.SaveChanges();
            Console.WriteLine("User added!");
            return 0;
        }



        public int DeleteUser(int id)
        {
            User obj = db.Users.Where(x => x.UserId == id).FirstOrDefault();
            if (obj != null)
            {
                db.Users.Remove(obj);
                Console.WriteLine("User deleted!");
                db.SaveChanges();
                return 0;
            }
            else
            {
                Console.WriteLine("No such user exists with this id");
                return 1;
            }
        }
           
        public int AddOrder(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return 0;
        }


        public List<Product> DisplayProduct()
        {

            List<Product> list = new List<Product>();
            list = db.Products.ToList();
            foreach (Product temp in list)
            {
                Console.WriteLine($"{temp.ProductName}--{temp.ProductDescription}");
            }

            return list;
        }
        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.ProductName == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.ProductName.Length > 0)
                    obj.ProductName = product.ProductName;
                

                if (product.ProductQty > 0)
                    obj.ProductQty = product.ProductQty;

                db.Products.Update(obj);
                db.SaveChanges();
            }
            else
            {
                return 1;
            }

            return 0;

        }

        public int PlaceOrder(Order order)
        {
            List<Product> products = db.Products.ToList();
            foreach (Product product in products)
            {
                Console.WriteLine(product.ProductId + " " + product.ProductName + " " +
                product.ProductQty);
            }
            Console.WriteLine("select product id");
            int prodId = Convert.ToInt32(Console.ReadLine());
            List<User> users = db.Users.Where(x => x.Role.RoleId == 2).ToList();
            foreach (User usr in users)
            {
                Console.WriteLine(usr.UserId + " " + usr.FirstName + " " +
                usr.LastName);
            }
            Console.WriteLine("select user id");
            int userId = Convert.ToInt32(Console.ReadLine());



            Product ProductObj = db.Products.Find(prodId);
            User UserObj = db.Users.Find(userId);



            order.Product = ProductObj;
            order.User = UserObj;



            db.Orders.Add(order);
            db.SaveChanges();
            Console.WriteLine("Order has been placed!");
            return 0;



        }

        




public List<User> GetUsers()

        {

            List<User> list = db.Users.ToList();

            return list;

        }



public List<Product> GetProducts()

        {

            List<Product> list = db.Products.ToList();



            return list;

        }












    }
}